# OpenML dataset: Al_Hilal-Archive-Scores

https://www.openml.org/d/43376

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
As Al Hilal football club is one of the most successful football clubs in Saudi Arabia, Middle East and Asia this dataset was an excercise for Web Scraping where i collected the archive of the results for the club. The dataset inclues all matches from 1996 to 2019 and many important matches from 1961 to 1995. The source where the data has been scraped of is Koora website which is one of te oldest arabic websites that specilize in football news and matches scores. 
Content
The dataset includes home team column which contains the team that hosts the match, the away team who came to play against the home team,  and between them the score of the match. After that the date where the game has been played in, then the wining team from the match. the column before the last contains the competition which the game has been a part of and the last column is the stage of the competition of the game. 
in home and away columns when the game is played in local competition the name of the team is written without the name of the country, but when the game is played in an international competiton the name of the team is followed by the country of the team as there could be teams which has the same name but from different countries. 
All column are object type except date column which is Datetime type. The dataset contains no null values except the stage column as the null value means that the match didn't have a stage such as friendly matches or the stage is not provided by the website 
Acknowledgements
Thanks to General Assembly  Misk Academy for the immersive data science course which taught many skills and methods one of which is Web Scraping that was used in this project. Thanks and apperciation goes to my instructors as they are always there for help and dedicate alot of time to ensure that all students master all skills. 
Inspiration
which years are the most successful ? 
which leagues the club scored the highest point ? 
what are the clubs that the Al Hilal won against the most and what are the teams that defated Al Hilal through the years ? 
could there be a model to predict the result of the feature matches for Al Hilal ? 
which months Al Hilal win in the most and which that Al Hilal lose in it ? 
and many exploerations that cone be done.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43376) of an [OpenML dataset](https://www.openml.org/d/43376). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43376/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43376/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43376/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

